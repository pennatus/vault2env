=========
Vault2Env
=========


.. image:: https://img.shields.io/pypi/v/vault2env.svg
        :target: https://pypi.python.org/pypi/vault2env

.. image:: https://img.shields.io/gitlab/pipeline/pennatus/vault2env/master   
        :alt: Gitlab pipeline status

.. image:: https://readthedocs.org/projects/vault2env/badge/?version=latest
        :target: https://vault2env.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Read from a KV-2 secret path in Vault and create a .env file from the list of key/value pairs.


* Free software: MIT license
* Documentation: https://vault2env.readthedocs.io.


Features
--------

* Reads environment secrets from a KV-2 path and outputs a .env file.

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
