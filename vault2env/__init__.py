# -*- coding: utf-8 -*-

"""Top-level package for Vault2Env."""

__author__ = """Steve Graham"""
__email__ = 'stgraham2000@gmail.com'
__version__ = '0.11.0'
